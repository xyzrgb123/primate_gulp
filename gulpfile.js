// {{{1 Настройки
const project = '../test/'

let name = {
	css: 'main',
	html: 'index',
	js: 'main'
};

let folder = {
	css: 'dist',
	html: 'dist',
	js: 'dist',
	stylus: 'src',
	pug: 'src',
	coffee: 'src'
};

let path = {
	src: function(fp, n, t) { return project + fp + '/' + n + '.' + t },
	dist: function(f) { return project + f },
	watch: function(fp, t) { return project + fp + '/**/*.' + t }
}

// {{{1 Константы
const {src, dest, series, watch} = require('gulp')
const stylus = require('gulp-stylus')
const pug = require('gulp-pug')
const coffee = require('gulp-coffee')
const csso = require('gulp-csso')
const include = require('gulp-file-include')
const htmlmin = require('gulp-htmlmin')
const sync = require('browser-sync').create()
const autoprefixer = require('gulp-autoprefixer')
const concat = require('gulp-concat')
const purify = require('gulp-purifycss')
const purgecss = require('gulp-purgecss')
const notify = require('gulp-notify')
const minify = require('gulp-minify')

// {{{1 Продакшн
function stylus_b() {
	return src(path.src(folder.stylus, name.css, 'styl'))
		.pipe(stylus())
		// .pipe(autoprefixer({browsers: ['last 2 versions']}))
		// .pipe(autoprefixer({cascade: false}))
		.pipe(autoprefixer())
        .on("error", notify.onError({
            message: "<%= error.message %>",
            title: "Stylus error!"
        }))
		.pipe(csso())
		.pipe(dest(path.dist(folder.css)))
}

function pug_b() {
	return src(path.src(folder.pug, name.html, 'pug'))
		.pipe(pug())
        .on("error", notify.onError({
            message: "<%= error.message %>",
            title: "pug error!"
        }))
		.pipe(htmlmin({
			// collapseWhitespace: true,
			// removeComments: true,
			collapseBooleanAttributes: true,
			removeScriptTypeAttributes: true,
			removeStyleLinkTypeAttributes: true,
			minifyJS: true,
			minifyCSS: true
		}))
		.pipe(dest(path.dist(folder.html)))
}

function coffee_b() {
	return src(path.src(folder.coffee, name.js, 'coffee'))
		.pipe(coffee())
        .on("error", notify.onError({
            message: "<%= error.message %>",
            title: "coffee error!"
        }))
		.pipe(minify())
		.pipe(dest(path.dist(folder.js)))
}

function purge_b() {
    return src(path.src(folder.css, name.css, 'css'))
        .pipe(purgecss({
            content: [path.src(folder.html, name.html, 'html')],
            rejected: false
        }))
        .pipe(dest(path.dist(folder.css)))
}

exports.build = series(stylus_b, pug_b, coffee_b, purge_b)

// {{{1 В производстве
function stylus_() {
	return src(path.src(folder.stylus, name.css, 'styl'))
		.pipe(stylus())
		// .pipe(autoprefixer({browsers: ['last 2 versions']}))
		// .pipe(autoprefixer({cascade: false}))
		.pipe(autoprefixer())
        .on("error", notify.onError({
            message: "<%= error.message %>",
            title: "Stylus error!"
        }))
		.pipe(dest(path.dist(folder.css)))
}

function pug_() {
	return src(path.src(folder.pug, name.html, 'pug'))
		.pipe(pug())
        .on("error", notify.onError({
            message: "<%= error.message %>",
            title: "pug error!"
        }))
		.pipe(dest(path.dist(folder.html)))
}

function coffee_() {
	return src(path.src(folder.coffee , name.js, 'coffee'))
		.pipe(coffee())
        .on("error", notify.onError({
            message: "<%= error.message %>",
            title: "coffee error!"
        }))
		.pipe(dest(path.dist(folder.js)))
}

function serve() {
	sync.init({ server: path.dist(folder.html) }),
	watch(path.watch(folder.stylus, 'styl'), series(stylus_)).on('change', sync.reload),
	watch(path.watch(folder.pug, 'pug'), series(pug_)).on('change', sync.reload),
	watch(path.watch(folder.coffee, 'coffee'), series(coffee_)).on('change', sync.reload)
}

exports.default = series(stylus_, pug_, coffee_, serve)
